//Setup the csrf token to every ajax call
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),

    }
});

$(function() {
	//Init the terminal
	$('#terminal').terminal(function(command, term) {
		term.pause();
		$.post(PUBLIC_URL + "/cube/command", {command: command}).then(function(response) {

			if( response.indexOf("BYE") == -1 ){
				
				term.echo(response).resume();

			} else {
				
				term.clear();
				term.echo(response).resume();
				term.echo('Welcome again to Cube Sumation! \nPlease type N° of test-cases.');

			}

			$("#responses").append(response + "<br>");
			
		})
		.fail(function() {
    	
    	term.echo("There was an error");

  	})
		.always(function() {
    	
    	term.resume();

  	});
  }, {
      greetings: 'Welcome to Cube Sumation! \nPlease type N° of test-cases.',
      onBlur: function() {
				return false;
      }
	});

})