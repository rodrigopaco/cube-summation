@extends('layout')

@section('css_extra')

	<link rel="stylesheet" type="text/css" href="{{ elixir('css/vendor.css') }}">

@stop

@section('content')
<div class="col-md-12">
	<div class="panel panel-primary">
		<div class="panel-heading">3D Matrix Test</div>
		<div class="panel-body">
			<div>
				<div class="row">
					<div class="col-md-6">
						<h2>Type commands</h2>
						<div id="terminal"></div>
					</div>
					<div class="col-md-6">
					<h2>Responses</h2>
						<div class="well" id="responses"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel-footer">By Eng. Rodrigo Paco</div>
	</div>
</div>
@endsection

@section('js_extra')

	<script type="text/javascript" src="{{ elixir('js/vendor.js') }}"></script>

@stop