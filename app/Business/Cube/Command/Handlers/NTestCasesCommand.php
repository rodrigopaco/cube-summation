<?php

namespace App\Business\Cube\Command\Handlers;

use App\Business\Cube\Command\ICommand;

class NTestCasesCommand implements ICommand
{

	public function onCommand( $command, $cubeSumation )
	{
		if ( $cubeSumation->getNTestCases() == 0 ){

			$nTest = intval($command);

			if( $cubeSumation->setNTestCases($nTest) ){

				echo ("OK. Now type the Matrix length and N° of operations. \n");

			} else {

				echo( $cubeSumation->getError() . " \n" );

			}

			return true;
		}

		return false;

	}

}