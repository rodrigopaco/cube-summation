<?php

namespace App\Business\Cube\Command\Handlers;

use App\Business\Cube\Command\ICommand;

class UpdateCommand implements ICommand
{

	const _COMMAND = "UPDATE";

	public function onCommand( $command, $cubeSumation )
	{
		//Parse command and args
		$parsedCommand  = explode(" ", $command);
		$name           = strtoupper( array_shift($parsedCommand) );
		$args			      = $parsedCommand;

		if ( $name == self::_COMMAND ){

			if( is_array($args) && count($args) == 4 ){

				list($x, $y, $z, $w) = $args;
				$resp = $cubeSumation->getCube()->setData($x, $y, $z, $w);

				if( $resp ){

					if( $cubeSumation->checkNextStep() ){

						$msg = ($cubeSumation->getCurrentOperation() == 0) ? "New Matrix setup" : "Matrix updated";
						echo  $msg . ". You have " . 
							($cubeSumation->getNOperations() - $cubeSumation->getCurrentOperation()) .
							" remain operations and " .
							($cubeSumation->getNTestCases() - $cubeSumation->getCurrentTestCase()) .
							" test-cases. \n";

					} else {

						$cubeSumation->clearAllData();
						echo( "BYE" );

					}

				} else {

					echo( "Error: " . $cubeSumation->getCube()->getError() . " \n" );

				}
			} else {
				
				echo( "Error: Parameters incorrect (x, y, z and W expected) \n" );
				
			}

			return true;
		}

		return false;
	}
}