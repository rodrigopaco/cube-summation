<?php

namespace App\Business\Cube\Command\Handlers;

use App\Business\Cube\Command\ICommand;

class QueryCommand implements ICommand
{
  
	const _COMMAND = "QUERY";

  public function onCommand( $command, $cubeSumation )
  {
    //Parse command and args
    $parsedCommand  = explode(" ", $command);
    $name           = strtoupper( array_shift($parsedCommand) );
    $args           = $parsedCommand;

    if ( $name == self::_COMMAND ){

      if( is_array($args) && count($args) == 6 ){

        list($x1, $y1, $z1, $x2, $y2, $z2) = $args;
        $resp = $cubeSumation->getCube()->getSum($x1, $y1, $z1, $x2, $y2, $z2);

        if( $resp !== false ){

          echo( "The sum is: " . $resp . " \n");

          if( !$cubeSumation->checkNextStep() ){

            $cubeSumation->clearAllData();
            echo( "BYE" );

          }

        } else {

          echo( "Error: " . $cubeSumation->getCube()->getError() . " \n" );

        }
      } else {
        
        echo( "Error: Parameters incorrect (x, y, z and W expected) \n" );
        
      }

      return true;
    }

    return false;
  }
}