<?php

namespace App\Business\Cube\Command\Handlers;

use App\Business\Cube\Command\ICommand;

class UnknowCommand implements ICommand
{
  
	const _COMMAND1 = "QUERY";
  const _COMMAND2 = "UPDATE";

  public function onCommand( $command, $cubeSumation )
  {
    //Parse command and args
		$parsedCommand  = explode(" ", $command);
		$name           = strtoupper( array_shift($parsedCommand) );
    
    if ( $name != self::_COMMAND1 || $name != self::_COMMAND2 ){
    	
      echo( "Unknow command. Please use QUERY or UPDATE command \n" );
    	return true;

    }
    
    return false;

  }
}