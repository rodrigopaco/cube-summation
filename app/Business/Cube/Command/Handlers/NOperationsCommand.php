<?php

namespace App\Business\Cube\Command\Handlers;

use App\Business\Cube\Command\ICommand;

class NOperationsCommand implements ICommand
{

  public function onCommand( $command, $cubeSumation )
  {

    if ( $cubeSumation->getNTestCases() > 0 && $cubeSumation->getCube()->getLength() == 0 ){

      //Parse command and args
      $parsedCommand  = explode(" ", $command);

      if( is_array($parsedCommand) && count($parsedCommand) == 2 ){

        $length         = $parsedCommand[0];
        $nOperations    = $parsedCommand[1];

        if( $cubeSumation->setCubeLengthAndNOperations($length, $nOperations) ){

          echo ("OK. Now you can make operations (QUERY and UPDATE). \n");

        } else {

          echo( $cubeSumation->getError() . " \n" );

        }

      } else {

        echo( "Error: Parameters incorrect (N and M expected) \n" );

      }

      return true;
    }

    return false;

  }

}