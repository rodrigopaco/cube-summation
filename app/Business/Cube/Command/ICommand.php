<?php

namespace App\Business\Cube\Command;

interface ICommand
{
  function onCommand( $command, $cubeSumation );
}