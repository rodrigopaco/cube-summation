<?php

namespace App\Business\Cube\Command;

class CommandChain
{
  private $_commands = array();
 
  public function addCommand( $cmd )
  {
    $this->_commands []= $cmd;
  }
 
  public function runCommand( $command, $cubeSumation )
  {
    foreach( $this->_commands as $cmd )
    {
      if ( $cmd->onCommand( $command, $cubeSumation ) )
        return;
    }
  }
}