<?php

namespace App\Business\Cube;

use Illuminate\Http\Request;

use App\Business\Cube\Command\ICommand;
use App\Business\Cube\Command\CommandChain;
use App\Business\Cube\Command\Handlers\NTestCasesCommand;
use App\Business\Cube\Command\Handlers\NOperationsCommand;
use App\Business\Cube\Command\Handlers\QueryCommand;
use App\Business\Cube\Command\Handlers\UpdateCommand;
use App\Business\Cube\Command\Handlers\UnknowCommand;

use App\Models\CubeSumation;

class CommandManager
{
    protected $commands;
    protected $cubeSumation;
    

    public function __construct(){
    	
    	//Init command chain classess
        $this->commands = new CommandChain();
        $this->commands->addCommand( new NTestCasesCommand() );
        $this->commands->addCommand( new NOperationsCommand() );
        $this->commands->addCommand( new QueryCommand() );
        $this->commands->addCommand( new UpdateCommand() );
        $this->commands->addCommand( new UnknowCommand() );

        $this->cubeSumation = new CubeSumation();
        
    }

    public function listen(Request $request){

        //Get the cube data from session and decode it
        $savedCubeSum = session('cubeSumation', false);
        $this->cubeSumation->setAllData( $savedCubeSum );

        //Get command from request
        $command = $request->input('command', "");

            //Run command
        $this->commands->runCommand( $command, $this->cubeSumation );

        \Session::put('cubeSumation', $this->cubeSumation->getAllData());
        \Session::save();

    }
}