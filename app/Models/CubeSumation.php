<?php

namespace App\Models;

use App\Models\Cube;

class CubeSumation
{
    protected $nTestCases = 0;
    protected $currenTestCase = 0;
    protected $nOperations = 0;
    protected $currenOperations = 0;
    protected $cube;
    protected $error;

    public function __construct(){

        $this->error = new Cube();

    }

    public function setNTestCases($nTestCases){
    	
    	if( $nTestCases >= 1 && $nTestCases <= 50 ){

            $this->nTestCases  = $nTestCases;
            return true;

        } else {

            $this->error = "Error: Parameter incorrect (number of test must be a number between 1 and 50)";
            return false;
        }

    }

    public function getNTestCases(){

        return $this->nTestCases;

    }

    public function setNOperations($nOperations){

        if( $nOperations >= 1 && $nOperations <= 1000 ){

            $this->nOperations = $nOperations;
            return true;

        } else {

            $this->error = "Error: Parameter incorrect (number of operations must be a number between 1 and 100
            .0)";
            return false;
        }

    }

    public function getNOperations(){

        return $this->nOperations;

    }

    public function incCurrentTestCase(){

        $this->currenTestCase = $this->currenTestCase + 1;

    }

    public function resetCurrentTestCase(){

        $this->currenTestCase = 0;

    }

    public function getCurrentTestCase(){

        return $this->currenTestCase;

    }

    public function incCurrentOperation(){

        $this->currenOperations = $this->currenOperations + 1;
        
    }

    public function resetCurrentOperation(){

        $this->currenOperations = 0;
        
    }

    public function getCurrentOperation(){

        return $this->currenOperations;

    }

    public function setCubeData($data){

        if( $data != null ){

            $this->cube->initData( $arrayData['cube'] );

        }

    }

    public function getCube(){

        return $this->cube;

    }

    public function setCubeLengthAndNOperations($length, $nOperations){

        if( $this->getCube()->setLenght($length) ){

            return $this->setNOperations($nOperations);

        } else {

            $this->error = $cubeSumation->getCube()->getError();

        }

        return false;
    }


    public function setAllData($arrayData){

        if( $arrayData ){

            $this->nTestCases  = $arrayData['nTestCases'];
            $this->currenTestCase = $arrayData['currenTestCase'];
            $this->nOperations = $arrayData['nOperations'];
            $this->currenOperations = $arrayData['currenOperations'];

            $this->cube  = new Cube();

            if( $arrayData['cubeLength'] > 0 ){

                $this->cube->setLenght( $arrayData['cubeLength'] );

            }

            if( $arrayData['cube'] != null ){

                $this->cube->initData( $arrayData['cube'] );

            }

        }

    }


    public function getAllData(){

        return [
            "nTestCases"        => $this->nTestCases,
            "currenTestCase"    => $this->currenTestCase,
            "nOperations"       => $this->nOperations,
            "currenOperations"  => $this->currenOperations,
            "cube"              => ($this->cube != null) ? $this->cube->getAllData() : null,
            "cubeLength"        => ($this->cube != null) ? $this->cube->getLength() : null,
        ];

    }

    public function clearAllData(){

        $this->nTestCases = 0;
        $this->currenTestCase = 0;
        $this->nOperations = 0;
        $this->currenOperations = 0;
        $this->cube = null;

    }

    public function checkNextStep(){

        $this->incCurrentOperation();
        
        if( $this->getCurrentOperation() <  $this->getNOperations() ){

            return true;

        } else {

            $this->resetCurrentOperation();
            $this->incCurrentTestCase();
            $this->getCube()->resetAll();
            
            if( $this->getCurrentTestCase() < ($this->getNTestCases() ) ){

                $this->getCube()->resetAll();
                return true;

            }

        }

        return false;

    }

    public function getError(){

        return $this->error;

    }

}
