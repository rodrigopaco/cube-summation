<?php

namespace App\Models;

class Cube
{
    protected $data = [];
    protected $length = 0;
    protected $error;

    public function initData($data){
    	
    	if($data){
    		$this->data = $data;
    	}

    }

    public function setLenght($length){

    	if( $length >= 1 && $length <= 100 ){
    		
    		$this->length = $length;
    		return true;

    	} else {

            $this->error = "The length of matrix should a number be between 1 and 100"; 
            return false;

        }

    }

    public function resetAll(){

        $this->length = 0;
        $this->data = null;
    }

    public function getLength(){

        return $this->length;

    }

    public function getError(){

    	return $this->error;

    }

    public function setData($x, $y, $z, $W){
		if( $x >= 1 && $x <= $this->length &&
				$y >= 1 && $y <= $this->length &&
				$z >= 1 && $z <= $this->length){
			
			if( $W >= -1000000000 && $W <= 1000000000 ){
					
				$this->data["$x,$y,$z"] = $W;
				return true;

			} else {

				$this->error = "Invalid value of W";

			}
				
		} else {

			$this->error = "Invalid position of x,y,z. Length of matrix is: " . $this->length;	

		}
			
		return false;
    }

    public function getSum($x1, $y1, $z1, $x2, $y2, $z2 ){
    	
        if( $x1 >= 1 && $x1 <= $x2 && $x1 <= $this->length &&
            $y1 >= 1 && $y1 <= $y2 && $y1 <= $this->length &&
            $z1 >= 1 && $x1 <= $z2 && $z1 <= $this->length &&
            $x2 <= $this->length &&
            $y2 <= $this->length &&
            $z2 <= $this->length)
        {
            
            $total = 0;
            
            foreach ($this->data as $key => $val)
            {
                $pos = explode(",", $key);

                if ( ($pos[0] >= $x1 && $pos[0] <= $x2) && 
                     ($pos[1] >= $y1 && $pos[1] <= $y2) && 
                     ($pos[2] >= $z1 && $pos[2] <= $z2))
                {
                    $total = $total  + $val;     
                }
            }

            return $total;

                
        } else {

            $this->error = "Invalid position of x1,y1,z1, x2, y2, z2. Length of matrix is: " . $this->length;

        }
            
        return false;

    }

    public function getAllData(){
    	return $this->data;
    }
}
