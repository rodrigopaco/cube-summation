<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Business\Cube\CommandManager;

class CubeController extends Controller
{
    protected $commandManager;

    public function __construct(){
    	
    	$this->commandManager = new CommandManager();
    }

    public function index(Request $request){
    	
    	$request->session()->forget('cubeSumation');
    	
    	return view('3DMatrix.index');

    }

    public function command(Request $request){
    	
		$this->commandManager->listen($request);
    	
    	exit();
			
    }
}
