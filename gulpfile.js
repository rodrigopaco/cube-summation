const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    /*mix.sass('app.scss')
       .webpack('app.js');*/

    mix.scripts([
	    		'../bower/jquery.terminal/js/jquery.terminal-0.11.23.js',
	    		'cube.js'
				], 'public/js/vendor.js')

    mix.styles([
        '../bower/jquery.terminal/css/jquery.terminal-0.11.23.css',
        'cube.css'
    	], 'public/css/vendor.css');

    mix.version(['public/js/vendor.js', 'public/css/vendor.css']);
});
